# Lets Hangman

### How to Run
Run the application from IDE.

**OR**

Run the application from terminal


For Mac `./gradlew run`

For Windows `gradlew.bat run`

### What to do

Follow the instructions from [Here](https://kiebot.notion.site/Hangman-game-ca261e49ce26451faddf6e0c35b5af31)