package in.upcode.hangman.error;

public class UnimplementedMethodException extends RuntimeException {
    public UnimplementedMethodException(String message) {
        super(message);
    }
}
